package de.hackathon_zwickau.shareshuttle_android

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.material.navigation.NavigationView


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    lateinit var drawer: DrawerLayout
    private lateinit var mGoogleSignIn: GoogleSignInAccount

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)


        drawer = findViewById(R.id.drawer_layout)
        val navigationView: NavigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)

        val toggle = ActionBarDrawerToggle(
            this,
            drawer,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(
                R.id.fragment_container,
                NavigationFragment()
            ).commit()
            navigationView.setCheckedItem(R.id.nav_navigation)
        }

        mGoogleSignIn = GoogleSignIn.getLastSignedInAccount(this)!!
        println(mGoogleSignIn.idToken)


        val name: AppCompatTextView = navigationView.getHeaderView(0).findViewById(R.id.nav_header_name)
        val email: AppCompatTextView = navigationView.getHeaderView(0).findViewById(R.id.nav_header_extra)
        val pic: AppCompatImageView = navigationView.getHeaderView(0).findViewById(R.id.nav_header_pic)

        DownloadImageTask(pic).execute(
            mGoogleSignIn.photoUrl.toString()
        )

        name.text = mGoogleSignIn.displayName
        email.text = mGoogleSignIn.email
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_navigation -> supportFragmentManager.beginTransaction().replace(
                R.id.fragment_container,
                NavigationFragment()
            ).commit()

            R.id.nav_my_routes -> supportFragmentManager.beginTransaction().replace(
                R.id.fragment_container,
                RouteOverviewFragment()
            ).commit()

            R.id.nav_settings -> supportFragmentManager.beginTransaction().replace(
                R.id.fragment_container,
                SettingsFragment()
            ).commit()

            else -> { // Note the block
                throw Exception("FOOOOOO")
            }
        }
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}








