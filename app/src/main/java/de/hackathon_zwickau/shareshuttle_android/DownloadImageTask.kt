package de.hackathon_zwickau.shareshuttle_android

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log
import android.widget.ImageView
import java.io.InputStream


class DownloadImageTask(private var bmImage: ImageView) : AsyncTask<String, Void, Bitmap>() {

    override fun doInBackground(vararg urls: String?): Bitmap {
        val urldisplay: String = urls[0]!!
        var mIcon11: Bitmap? = null
        try {
            val inp: InputStream = java.net.URL(urldisplay).openStream()
            mIcon11 = BitmapFactory.decodeStream(inp)
        } catch (e: Exception) {
            Log.e("Error", e.message)
            e.printStackTrace()
        }
        return mIcon11!!
    }

    override fun onPostExecute(result: Bitmap?) {
        bmImage.setImageBitmap(result)
    }
}