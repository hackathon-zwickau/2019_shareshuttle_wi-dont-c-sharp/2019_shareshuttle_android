package de.hackathon_zwickau.shareshuttle_android

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val mGoogleSignIn = GoogleSignIn.getLastSignedInAccount(this)

        val activityIntent: Intent = if (mGoogleSignIn == null) {
            Intent(this, LoginActivity::class.java)
        } else {
            Intent(this, MainActivity::class.java)
        }
        startActivity(activityIntent)
        finish()
    }
}
