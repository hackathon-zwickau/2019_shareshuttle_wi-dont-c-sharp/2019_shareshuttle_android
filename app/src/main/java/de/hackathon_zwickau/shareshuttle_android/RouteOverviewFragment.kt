package de.hackathon_zwickau.shareshuttle_android

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class RouteOverviewFragment : Fragment() {
    //LIST OF ARRAY STRINGS WHICH WILL SERVE AS LIST ITEMS
    var listItems = ArrayList<String>()

    //DEFINING A STRING ADAPTER WHICH WILL HANDLE THE DATA OF THE LISTVIEW
    lateinit var adapter: ArrayAdapter<String>

    //RECORDING HOW MANY TIMES THE BUTTON HAS BEEN CLICKED
    var clickCounter = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_route_overview, container, false)
    }

    override fun onStart() {
        super.onStart()
        val list = activity!!.findViewById(R.id.list) as RecyclerView
        val arrayList = ArrayList<String>()

        //DEBUGGING
        val r1 = Route()
        r1.weekday = "mo"
        r1.countPerMonth = "2"
        r1.destination = "München"
        r1.earnings = "0$"
        r1.start = "Berlin"
        r1.time = "17.ooUhr"
        val r2 = Route()
        r2.weekday = "DI"
        r2.countPerMonth = "4"
        r2.destination = "Leipzig"
        r2.earnings = "100$"
        r2.start = "Bremen"
        r2.time = "12.ooUhr"
        val r3 = Route()


        val mAdapter = MyRecyclerViewAdapter(this.requireContext(), listOf<Route>(r1, r2))
        list.adapter = mAdapter
        list.layoutManager = LinearLayoutManager(this.context)

        // Here, you set the data in your ListView


    }


    //METHOD WHICH WILL HANDLE DYNAMIC INSERTION
    fun addItems(s: String) {
        listItems.add("Clicked : " + clickCounter++)
        adapter.notifyDataSetChanged()
    }
}

class Route {
    var weekday = ""
    var time = ""
    var countPerMonth = ""
    var start = ""
    var destination = ""
    var earnings = ""
}

class MyRecyclerViewAdapter(
    private val mContext: Context,
    private val feedItemList: List<Route>?
) : RecyclerView.Adapter<MyRecyclerViewAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): CustomViewHolder {
        val view =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.route_item, viewGroup, false)
        return CustomViewHolder(view)
    }

    override fun onBindViewHolder(customViewHolder: CustomViewHolder, i: Int) {
        val feedItem = feedItemList!![i]

        //Setting text view title
        customViewHolder.weekday.text = feedItem.weekday
        customViewHolder.time.text = feedItem.time
        //customViewHolder.countPerMonth.text = feedItem.countPerMonth
        customViewHolder.start.text = feedItem.start + " --> " + feedItem.destination
        customViewHolder.earnings.text = feedItem.earnings
    }

    override fun getItemCount(): Int {
        return feedItemList?.size ?: 0
    }

    class CustomViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var weekday: TextView
        var time: TextView
        //var countPerMonth : TextView
        var start: TextView
        //var destination : TextView
        var earnings: TextView

        init {
            this.weekday = view.findViewById<TextView>(R.id.weekday) as TextView
            this.time = view.findViewById<TextView>(R.id.time) as TextView
            //this.countPerMonth = view.findViewById<TextView>(R.id.countPerMonth) as TextView
            this.start = view.findViewById<TextView>(R.id.route) as TextView
            //this.destination = view.findViewById<TextView>(R.id.destination) as TextView
            this.earnings = view.findViewById<TextView>(R.id.earnings) as TextView
        }
    }
}