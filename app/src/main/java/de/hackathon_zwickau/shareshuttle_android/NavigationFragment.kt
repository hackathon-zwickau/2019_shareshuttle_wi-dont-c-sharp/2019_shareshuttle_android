package de.hackathon_zwickau.shareshuttle_android

import android.Manifest
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.Toast
import androidx.constraintlayout.widget.Constraints.TAG
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import java.io.IOException


class NavigationFragment : Fragment(), OnMapReadyCallback,
    GoogleApiClient.OnConnectionFailedListener {
    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var mMap: GoogleMap
    private lateinit var mSearchText: AutoCompleteTextView
    private lateinit var mPlaceAutocompleteAdapter: PlaceAutocompleteAdapter
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private val latLngBounds = LatLngBounds(LatLng(44.0, 16.0), LatLng(56.0, 4.0))

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_navigation, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isMyLocationButtonEnabled = false
        mMap.uiSettings.isCompassEnabled = false
        mMap.uiSettings.isTiltGesturesEnabled = false
        mMap.uiSettings.isRotateGesturesEnabled = false
        mMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    50.887829,
                    10.37037
                ), 5.8f
            )
        )


        val permissionAccessFineLocationApproved = ActivityCompat
            .checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED

        if (permissionAccessFineLocationApproved) {
            onLocationAccessGranted()
        } else {
            // Make a request for foreground-only location access.
            ActivityCompat.requestPermissions(
                this.activity!!,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                1337
            )
        }


//        val mGoogleApiClient = GoogleApiClient.Builder(context!!)
//            .addApi(Places.GEO_DATA_API)
//            .addApi(Places.PLACE_DETECTION_API)
//            .build()

//        mPlaceAutocompleteAdapter = PlaceAutocompleteAdapter(context!!, mGoogleApiClient, latLngBounds, null!!)
//
//        mSearchText = (AutoCompleteTextView) childFragmentManager.view.findFragmentById(R.id.map)!!.view!!.findViewById(
//                R.id.input_search
//            )
//
//        mSearchText.setAdapter(mPlaceAutocompleteAdapter)
//
//        mSearchText.setOnEditorActionListener(object : TextView.OnEditorActionListener {
//            override fun onEditorAction(
//                textView: TextView,
//                actionId: Int,
//                keyEvent: KeyEvent
//            ): Boolean {
//                if (actionId == EditorInfo.IME_ACTION_SEARCH
//                    || actionId == EditorInfo.IME_ACTION_DONE
//                    || keyEvent.getAction() === KeyEvent.ACTION_DOWN
//                    || keyEvent.getAction() === KeyEvent.KEYCODE_ENTER
//                ) {
//
//                    //execute our method for searching
//                    geoLocate()
//                }
//
//                return false
//            }
//        })
//
//
//
    }


    private fun geoLocate() {
        val searchString: String = mSearchText.text.toString()

        val geocoder: Geocoder = Geocoder(context!!)
        var list: List<Address> = ArrayList()
        try {
            list = geocoder.getFromLocationName(searchString, 1)
        } catch (e: IOException) {
            Log.e(TAG, "geoLocate: IOException: " + e.message)
        }

        if (list.size > 0) {
            val address = list[0]

            Log.d(TAG, "geoLocate: found a location: $address")
            //Toast.makeText(this, address.toString(), Toast.LENGTH_SHORT).show();

            mMap.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(address.latitude, address.longitude), 11f
                )
            )
        }
    }

    private fun onLocationAccessGranted() {
        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location != null) {
                    val myLatLng = LatLng(
                        location.latitude,
                        location.longitude
                    )
                    val myPosition = CameraPosition.Builder()
                        .target(myLatLng).zoom(11f).bearing(0f).tilt(0f).build()
                    mMap.animateCamera(
                        CameraUpdateFactory.newCameraPosition(myPosition),
                        1300,
                        null
                    )
                } else {
                    Toast.makeText(
                        this.context,
                        "Standort kann nicht bestimmt werden...",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
    }
}
